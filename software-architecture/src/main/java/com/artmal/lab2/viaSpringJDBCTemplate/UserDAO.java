package com.artmal.lab2.viaSpringJDBCTemplate;

import javax.sql.DataSource;
import java.util.List;

public interface UserDAO {
    public void setDataSource(DataSource ds);

    public void create(String username, String password);

    public User getUser(int id);
    public List<User> listUsers();

    public void update(Integer id, String username);

    public void delete(int id);

}

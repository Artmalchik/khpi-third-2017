package com.artmal.lab2.viaSpringData.service;

import com.artmal.lab2.viaSpringData.User;
import com.artmal.lab2.viaSpringData.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserServiceImpl implements UserService{
    @Autowired
    private UserDAO userDAO;

    public void save(User user) {
        userDAO.save(user);
    }

    public User getUser(long id) {
        return userDAO.findOne(id);
    }

    public List<User> listUsers() {
        return userDAO.findAll();
    }

    public void update(long id, String username) {
        User userFromDB = userDAO.findOne(id);
        userFromDB.setUsername(username);
        userDAO.save(userFromDB);
    }

    public void delete(long id) {
        userDAO.delete(id);
    }
}

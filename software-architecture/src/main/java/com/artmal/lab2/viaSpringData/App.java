package com.artmal.lab2.viaSpringData;

import com.artmal.lab2.viaSpringData.service.UserService;
import com.artmal.lab2.viaSpringData.service.UserServiceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.List;

public class App {
    private ApplicationContext context;
    private UserService userService;

    private App() {
        context = new FileSystemXmlApplicationContext
                ("/src/main/java/com/artmal/lab2/viaSpringData/config/appconfig-root.xml");

        userService = context.getBean(UserServiceImpl.class);
    }

    public static void main(String[] args) {
        App app = new App();

        app.doCreate();
        app.doRead();
        app.doUpdate();
        app.doDelete();
    }

    private void doCreate() {
        System.out.println("~~~ Create ~~~");
        User user = new User("Artmal", "strongPassword");
        userService.save(user);
    }

    private void doRead() {
        System.out.println("~~~ Read ~~~");
        List<User> users = userService.listUsers();
        for(User user : users) {
            System.out.println(user);
        }
    }

    private void doUpdate() {
        System.out.println("~~~ Update ~~~");
        userService.update(1, "NeArtmal");
    }

    private void doDelete() {
        System.out.println("~~~ Delete ~~~");
        userService.delete(2);
    }
}

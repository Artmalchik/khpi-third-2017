package com.artmal.lab2.viaSpringData.service;

import com.artmal.lab2.viaSpringData.User;

import java.util.List;

public interface UserService {
    public void save(User user);

    public User getUser(long id);
    public List<User> listUsers();

    public void update(long id, String username);

    public void delete(long id);
}

package com.artmal.lab1.viaAnnotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class Executor {
    @Autowired
    private Hello helloService;

    public static void main(String[] args) {
        ApplicationContext context = new FileSystemXmlApplicationContext
                ("/src/main//java/com/artmal/lab1/viaAnnotations/appconfig-root.xml");

        Executor executor = context.getBean(Executor.class);
        executor.executeHello();
    }

    private void executeHello() {
        helloService.sayHello();
    }
}

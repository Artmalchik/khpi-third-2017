package com.artmal.lab1.viaAnnotations;

import org.springframework.stereotype.Component;

@Component
public class HelloService implements Hello {
    public void sayHello() {
        System.out.println("Say Hello World");
    }
}

package com.artmal.lab1.viaXML;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class Executor {
    public static void main(String[] args) {
        Executor executor = new Executor();
        executor.executeHello();
    }

    private void executeHello() {
        ApplicationContext context = new FileSystemXmlApplicationContext
                ("src/main/java/com/artmal/lab1/viaXML/appconfig-root.xml");

        Hello helloService = (Hello) context.getBean("HelloService");
        helloService.sayHello();
    }
}